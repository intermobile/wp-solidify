# WP Solidify

A library that uses the best OOP practices to provide a solid project structure for component-based WordPress themes.

-   [Installation](#installation)
-   [Setup](#setup)
-   [Handbook](#handbook)
    -   [Registering a new post type](#registering-a-new-post-type)
    -   [Creating a new custom fields group](#creating-a-new-custom-fields-group)

## Installation

WP Solidify is a [Composer](https://getcomposer.org/) package, and it's avaliable through [Packagist](https://packagist.org/packages/intermobile/wp-solidify).

For installing it into your project, run the command below.

```
composer require intermobile/wp-solidify
```

## Setup

In the `composer.json` file, setup a folder inside your theme as the root namespace.

```json
{
	"autoload": {
		"psr-4": {
			"App\\": "src/"
		}
	}
}
```

Init Solidify in your `functions.php`.

```php
use Solidify\Core\Theme;
use Solidify\Core\WPTemplate;

// Composer autoload
require get_template_directory() . '/vendor/autoload.php';

$registrable_namespaces = [];

// Check if ACF plugin is active to register fields
if (function_exists('acf_add_local_field_group')) {
	$registrable_namespaces[] = 'FieldsGroup';
	$registrable_namespaces[] = 'Options';
}

// Set core registrables
$registrable_namespaces = array_merge(
	$registrable_namespaces,
	array(
		'Taxonomies',
		'PostTypes',
		'Hooks',
	)
);

// Setup a theme instance for Solidify
global $theme_class;
$theme_class = new Theme(
	array(
		'template_engine' => new WPTemplate(),
		'namespace' => 'App',
		'base_folder' => 'src',
		'registrable_namespaces' => $registrable_namespaces,
		'theme_name' => 'wp-solidify-theme',
	)
);
```

> You can check [this theme](https://gitlab.com/intermobile/wp-boilerplate) as a reference.

## Handbook

### Registrables

The `PostType`, `Taxonomy`, and `FieldGroup` classes extend the `Registrable` interface, those classes must have a constructor method that will be automatically get called at site startup.

### Registering a new post type

1. Create a new class inside the `PostTypes` namespace.

2. Set `post_type` and `args` properties inside the constructor method, those properties will be forwarded to `register_post_type` function.

> See `register_post_type` [docs](https://developer.wordpress.org/reference/functions/register_post_type/) to see more details about the `args` property.

**Example**

Registering a new post type called **Products**.

```php
// # src/PostTypes/Products.php

namespace App\PostTypes;

use Solidify\Core\PostType;

class Products extends PostType{
	public function __construct()
	{
		$this->post_type = "product";

		$labels = [
			"name" => "Products",
			"singular_name" => "Product",
			"menu_name" => "Products",
			"all_items" => "All Products",
			"add_new" => "Add new",
			"add_new_item" => "Add new product",
			"edit_item" => "Edit product",
			"new_item" => "New product",
			"view_item" => "View proct",
			"insert_into_item" => "Insert in product",
			"view_items" => "View products",
			"search_items" => "Search for products",
			"not_found" => "No products found",
			"not_found_in_trash" => "No products found in trash"
		];

		$this->args = [
			"label"               => "Products",
			"labels"              => $labels,
			"description"         => "",
			"public"              => true,
			"publicly_queryable"  => false,
			"show_ui"             => true,
			"show_in_rest"        => false,
			"rest_base"           => "",
			"has_archive"         => false,
			"show_in_menu"        => true,
			"exclude_from_search" => true,
			"capability_type"     => "post",
			"map_meta_cap"        => true,
			"hierarchical"        => false,
			"menu_position"       => 8,
			'rewrite'             => array("slug" => $this->post_type, "with_front" => false),
			'query_var'           => false,
			"supports"            => array("title", "editor", "revisions", "excerpt"),
			"menu_icon"           => 'dashicons-book-alt',
			"taxonomies"          => []
		];
	}
}

```

### Creating a new custom fields group

1. Create a new class inside the `FieldsGroup` namespace, call `set_fields` and set `args` inside the class constructor.

2. The `set_fields` method receives has an array as an argument, the array key is the field name, and the value is a `Field` class instance.

**Example**

Creating a new fields group to `product` post type.

```php
// # src/FieldsGroup/Product.php

namespace App\FieldsGroup;

use Solidify\Core\FieldGroup;
use Solidify\Core\PostType;
use Solidify\Fields;

class Product extends FieldGroup{
	public function __construct() {
		// Set fields
		$this->set_fields([
			'product_detailts_tab' => new Fields\Tab('Product Details'),
			'value' => new Fields\Number('Value'),
			'subtitle' => new Fields\Text('Subtitle', [
				// You can pass an acf options array as the second argument
				'wrapper' => [
					'width' => 70
				]
			]),
			'gallery_thumbnail' => new Fields\Image('Gallery Thumbnail', [
				'wrapper' => [
					'width' => 30
				]
			])
		]);

		// Set arguments
		$this->args = [
			'key' => 'product-fields',
			'title' => 'Product Fields',
			'location' => [
				[
					// Pages, Taxonomies, OptionsPages, and PostTypes
					// classes have static methods for acf conditionals.
					PostType::is_equal_to('product'),
				],
			]
		];
	}
}

```
