<?php

namespace Solidify\Interfaces;

interface Registrable {
	public function register(): void;
}
