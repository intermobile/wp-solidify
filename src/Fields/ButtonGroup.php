<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * ButtonGroup field type
 *
 * @example src/Examples/ButtonGroup.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/ButtonGroup.inc
 *
 * @see https://www.advancedcustomfields.com/resources/button-group/
 */
class ButtonGroup extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'button_group' );
}