<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Taxonomy field type
 *
 * @example src/Examples/Taxonomy.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Taxonomy.inc
 *
 * @see https://www.advancedcustomfields.com/resources/taxonomy/
 */
class Taxonomy extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'taxonomy' );
}