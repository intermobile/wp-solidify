<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * User field type
 *
 * @example src/Examples/User.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/User.inc
 *
 * @see https://www.advancedcustomfields.com/resources/user/
 */
class User extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'user' );
}