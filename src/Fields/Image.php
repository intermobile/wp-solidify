<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Image field type
 *
 * @example src/Examples/Image.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Image.inc
 *
 * @see https://www.advancedcustomfields.com/resources/image/
 */
class Image extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'image' );
}