<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Message field type
 *
 * @example src/Examples/Message.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Message.inc
 */
class Message extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'message' );
}