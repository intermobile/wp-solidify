<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Generic field type
 */
class Generic extends Field {
	/**
	 * Default properties for this field.
	 * Default it's empty because it's fully configurable
	 *
	 * @var array
	 */
	public $defaults = array();
}