<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Clone field type
 *
 * @example src/Examples/CloneField.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/CloneField.inc
 */
class CloneField extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'clone' );
}