<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Gallery field type
 *
 * @example src/Examples/Gallery.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Gallery.inc
 *
 * @see https://www.advancedcustomfields.com/resources/gallery/
 */
class Gallery extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'gallery' );
}