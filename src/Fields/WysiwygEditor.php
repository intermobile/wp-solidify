<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * WYSIWYG Editor field type
 *
 * @example src/Examples/WysiwygEditor.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/WysiwygEditor.inc
 *
 * @see https://www.advancedcustomfields.com/resources/wysiwyg-editor/
 */
class WysiwygEditor extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'wysiwyg' );
}