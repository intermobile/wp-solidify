<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Password field type
 *
 * @example src/Examples/Password.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Password.inc
 */
class Password extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'password' );
}