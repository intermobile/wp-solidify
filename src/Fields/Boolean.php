<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Boolean field type
 * 
 * @example src/Examples/Boolean.inc How to use this function
 * 
 * @see https://www.advancedcustomfields.com/resources/true-false/
 *
 * @deprecated Deprecated since version 1.0.3. Use the TrueFalse type instead.
*/
class Boolean extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'true_false' );
}
