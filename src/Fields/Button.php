<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Button custom field type
 *
 * @example src/Examples/Button.inc How to use this function
 *
 * A custom field type used for set up of link buttons
 *
 * @deprecated Deprecated since version 1.0.3. Use the Link type instead.
 */
class Button extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array(
		'type'   => 'group',
		'layout' => 'table',
	);

	/**
	 * Define a group of fields that will be generated
	 *
	 * @param string $label Field label.
	 * @param array  $args Customized args.
	 */
	public function __construct( $label, $args = array() ) {
		parent::__construct( $label, $args );

		$this->args['sub_fields'] = array(
			'content'    => new Text( 'Conteúdo' ),
			'url'        => new URL( 'URL' ),
			'new_window' => new Boolean( 'Open in new window?' ),
		);
	}
}
