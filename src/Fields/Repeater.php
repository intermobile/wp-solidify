<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Repeater field type
 *
 * @example src/Examples/Repeater.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Repeater.inc
 *
 * @see https://www.advancedcustomfields.com/resources/repeater/
 */
class Repeater extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'repeater' );
}