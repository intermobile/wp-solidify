<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Relationship field type
 *
 * @example src/Examples/Relationship.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Relationship.inc
 *
 * @see https://www.advancedcustomfields.com/resources/relationship/
 */
class Relationship extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'relationship' );
}