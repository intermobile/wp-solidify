<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Page Link field type
 *
 * @example src/Examples/PageLink.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/PageLink.inc
 *
 * @see https://www.advancedcustomfields.com/resources/page-link/
 */
class PageLink extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'page_link' );
}