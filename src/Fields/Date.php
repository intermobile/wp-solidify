<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Date field type
 *
 * @example src/Examples//**.inc How to use this function
 *
 * @see https://www.advancedcustomfields.com/resources/date-picker/
 *
 * @deprecated Deprecated since version 1.0.3. Use the DatePicker type instead.
 */
class Date extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'date_picker' );
}
