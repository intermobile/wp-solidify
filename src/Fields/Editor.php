<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Editor field type
 *
 * @example src/Examples/Editor.inc How to use this function
 *
 * @see https://www.advancedcustomfields.com/resources/wysiwyg-editor/
 *
 * @deprecated Deprecated since version 1.0.3. Use the WysiwygEditor type instead.
 */
class Editor extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'wysiwyg' );
}
