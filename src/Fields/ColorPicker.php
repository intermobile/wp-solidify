<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Color Picker field type
 *
 * @example src/Examples/ColorPicker.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/ColorPicker.inc
 *
 * @see https://www.advancedcustomfields.com/resources/color-picker/
 */
class ColorPicker extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'color_picker' );
}