<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Tab field type
 *
 * @example src/Examples/Tab.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Tab.inc
 *
 * @see https://www.advancedcustomfields.com/resources/tab/
 */
class Tab extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'tab' );
}