<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * True/False field type
 *
 * @example src/Examples/TrueFalse.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/TrueFalse.inc
 *
 * @see https://www.advancedcustomfields.com/resources/true-false/
 */
class TrueFalse extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'true_false' );
}