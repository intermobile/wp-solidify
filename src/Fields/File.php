<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * File field type
 *
 * @example src/Examples/File.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/File.inc
 *
 * @see https://www.advancedcustomfields.com/resources/file/
 */
class File extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'file' );
}