<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Layout field type
 *
 * @example src/Examples/Layout.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Layout.inc
 */
class Layout extends Field {
}