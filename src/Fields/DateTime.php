<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Date Time field type
 *
 * @example src/Examples/DateTime.inc How to use this function
 *
 * @see https://www.advancedcustomfields.com/resources/date-time-picker/
 *
 * @deprecated Deprecated since version 1.0.3. Use the DateTimePicker type instead.
 */
class DateTime extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'date_time_picker' );
}
