<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Flexible field type
 *
 * @example src/Examples/Flexible.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Flexible.inc
 *
 * @see https://www.advancedcustomfields.com/resources/flexible/
 */
class Flexible extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'flexible_content' );
}