<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Time Picker field type
 *
 * @example src/Examples/TimePicker.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/TimePicker.inc
 *
 * @see https://www.advancedcustomfields.com/resources/time-picker/
 */
class TimePicker extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'time_picker' );
}