<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Radio field type
 *
 * @example src/Examples/Radio.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Radio.inc
 *
 * @see https://www.advancedcustomfields.com/resources/radio/
 */
class Radio extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'radio' );
}