<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Textarea field type
 *
 * @example src/Examples/Textarea.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Textarea.inc
 *
 * @see https://www.advancedcustomfields.com/resources/textarea/
 */
class Textarea extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'textarea' );
}