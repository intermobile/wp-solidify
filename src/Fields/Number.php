<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Number field type
 *
 * @example src/Examples/Number.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Number.inc
 */
class Number extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'number' );
}