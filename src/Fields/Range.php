<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Range field type
 *
 * @example src/Examples/Range.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Range.inc
 *
 * @see https://www.advancedcustomfields.com/resources/range/
 */
class Range extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'range' );
}