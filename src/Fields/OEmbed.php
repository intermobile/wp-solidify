<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * OEmbed field type
 *
 * @example src/Examples/OEmbed.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/OEmbed.inc
 *
 * @see https://www.advancedcustomfields.com/resources/oembed/
 */
class OEmbed extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'oembed' );
}