<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Select field type
 *
 * @example src/Examples/Select.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Select.inc
 *
 * @see https://www.advancedcustomfields.com/resources/select/
 */
class Select extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'select' );
}