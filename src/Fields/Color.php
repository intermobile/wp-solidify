<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Color field type
 *
 * @example src/Examples/Color.inc How to use this function
 *
 * @see https://www.advancedcustomfields.com/resources/color-picker/
 *
 * @deprecated Deprecated since version 1.0.3. Use the ColorPicker type instead.
 */
class Color extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'color_picker' );
}
