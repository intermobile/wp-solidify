<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Time field type
 *
 * @example src/Examples/Time.inc How to use this function
 *
 * @see https://www.advancedcustomfields.com/resources/time-picker/
 *
 * @deprecated Deprecated since version 1.0.3. Use the TimePicker type instead.
 */
class Time extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'time_picker' );
}
