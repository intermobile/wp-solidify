<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Checkbox field type
 *
 * @example src/Examples/Checkbox.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Checkbox.inc
 *
 * @see https://www.advancedcustomfields.com/resources/checkbox/
 */
class Checkbox extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'checkbox' );
}