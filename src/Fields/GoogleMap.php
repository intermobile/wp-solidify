<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Google Map field type
 *
 * @example src/Examples/GoogleMap.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/GoogleMap.inc
 *
 * @see https://www.advancedcustomfields.com/resources/google-map/
 */
class GoogleMap extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'google_map' );
}