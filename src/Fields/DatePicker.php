<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Date Picker field type
 *
 * @example src/Examples/DatePicker.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/DatePicker.inc
 *
 * @see https://www.advancedcustomfields.com/resources/date-picker/
 */
class DatePicker extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'date_picker' );
}