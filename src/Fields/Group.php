<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Group field type
 *
 * @example src/Examples/Group.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Group.inc
 *
 * @see https://www.advancedcustomfields.com/resources/group/
 */
class Group extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'group' );
}