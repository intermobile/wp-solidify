<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Date Time Picker field type
 *
 * @example src/Examples/DateTimePicker.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/DateTimePicker.inc
 *
 * @see https://www.advancedcustomfields.com/resources/date-time-picker/
 */
class DateTimePicker extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'date_time_picker' );
}