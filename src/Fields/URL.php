<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * URL field type
 *
 * @example src/Examples/URL.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/URL.inc
 */
class URL extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'url' );
}