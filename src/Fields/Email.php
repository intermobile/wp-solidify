<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Email field type
 *
 * @example src/Examples/Email.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Email.inc
 */
class Email extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'email' );
}