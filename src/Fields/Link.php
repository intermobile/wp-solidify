<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Link field type
 *
 * @example src/Examples/Link.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Link.inc
 *
 * @see https://www.advancedcustomfields.com/resources/link/
 */
class Link extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'link' );
}