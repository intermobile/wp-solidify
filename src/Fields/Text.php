<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Text field type
 *
 * @example src/Examples/Text.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Text.inc
 *
 * @see https://www.advancedcustomfields.com/resources/text/
 */
class Text extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'text' );
}