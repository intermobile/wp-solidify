<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Post Object field type
 *
 * @example src/Examples/PostObject.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/PostObject.inc
 *
 * @see https://www.advancedcustomfields.com/resources/post-object/
 */
class PostObject extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'post_object' );
}