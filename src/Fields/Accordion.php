<?php

namespace Solidify\Fields;

use Solidify\Core\Field;

/**
 * Accordion field type
 *
 * @example src/Examples/Accordion.inc How to use this class
 *
 * @see https://gitlab.com/intermobile/wp-solidify/-/blob/master/src/Examples/Accordion.inc
 *
 * @see https://www.advancedcustomfields.com/resources/accordion/
 */
class Accordion extends Field {
	/**
	 * Default properties for this field.
	 *
	 * @var array
	 */
	public $defaults = array( 'type' => 'accordion' );
}