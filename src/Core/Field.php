<?php

namespace Solidify\Core;

/**
 * Creates a new interface to ACF Field
 *
 * This class must be extended and files must be in /Solidify/Core/Fields directory
 *
 * @param string $label Text to appear as the field label in the CMS
 * @param array $args An array of ACF field arguments
 *
 * @see https://www.advancedcustomfields.com/resources/register-fields-via-php/
 */
abstract class Field {
	public $args     = array();
	public $defaults = array();

	public function __construct( $label, $args = array() ) {
		$args['label'] = $label;
		$this->args    = array_merge( $args, $this->defaults );
	}

	/**
	 * Get field args that will be used in acf_add_local_field_group function.
	 *
	 * @param string $group_key - field group key, will be prepended to field key.
	 * @param string $field_name - field name prefixed by field group key will be used as key.
	 * @return array
	 */
	public function get_args( $group_key, $field_name ) {
		$this->args['name'] = $field_name;
		$this->args['key']  = isset( $this->args['key'] ) ? $this->args['key'] : $group_key . '_' . $field_name;

		$args = $this->args;

		if ( isset( $this->args['sub_fields'] ) && $this->args['sub_fields'] ) {
			$args['sub_fields'] = self::get_fields_args(
				$this->args['sub_fields'],
				$this->args['key']
			);
		}

		if ( isset( $this->args['layouts'] ) && $this->args['layouts'] ) {
			$args['layouts'] = self::get_fields_args(
				$this->args['layouts'],
				$this->args['key']
			);
		}

		return (array) $args;
	}

	/**
	 * Get args from multiple Field instances in array.
	 *
	 * @param array  $fields List of Field instances.
	 * @param string $parent_key Parent key.
	 * @return array
	 */
	public static function get_fields_args( $fields, $parent_key ) {
		$fields_args = array();

		foreach ( $fields as $field_name => $field ) {
			$fields_args[] = $field->get_args( $parent_key, $field_name );
		}

		return $fields_args;
	}

	public function get_value( string $field_key, array $parent ) {
		return get_field( $field_key );
	}
}