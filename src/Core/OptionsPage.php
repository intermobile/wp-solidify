<?php

namespace Solidify\Core;

use Solidify\Interfaces\Registrable;

/**
 * Creates a new interface to acf options page.
 *
 * This class must be extended and files must be in /Site/Options directory.
 */
abstract class OptionsPage implements Registrable {
	/**
	 * ACF args for acf_add_options_page or acf_add_options_sub_page
	 *
	 * @see https://www.advancedcustomfields.com/resources/acf_add_options_page/#parameters
	 * @see https://www.advancedcustomfields.com/resources/acf_add_options_sub_page/#parameters
	 *
	 * @var array
	 */
	protected $args = array();

	/**
	 * ACF acf_add_options_page args
	 *
	 * @see https://www.advancedcustomfields.com/resources/options-page/#advanced-usage
	 *
	 * @var bool
	 */
	protected $is_sub_page = false;

	/**
	 * Register new ACF options page.
	 *
	 * @see https://www.advancedcustomfields.com/resources/acf_add_options_page/
	 *
	 * @return void
	 */
	public function register(): void {
		if ( $this->is_sub_page ) {
			acf_add_options_sub_page( $this->args );
		} else {
			acf_add_options_page( $this->args );
		}
	}

	/**
	 * Helper function to return conditional arguments based on options page.
	 *
	 * @param string $options_page - string containing the options_page.
	 * @return array conditional array
	 */
	public static function is_equal_to( string $options_page ): array {
		return array(
			'param'    => 'options_page',
			'operator' => '==',
			'value'    => $options_page,
		);
	}
}