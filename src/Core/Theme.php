<?php

namespace Solidify\Core;

use Error;

class Theme { // phpcs:ignore
	public $template_engine;
	public $namespace;
	public $base_folder;
	public $registrable_namespaces;
	public $registrable_namespaces_for_acf;
	public $theme_name;

	/**
	 * Template engine
	 *
	 * @param array $args [
	 *  'template_engine'           => @param TemplateEngine
	 *  'namespace'                 => @param string
	 *  'base_folder'               => @param string
	 *  'registrable_namespaces'    => @param string
	 * ].
	 *
	 * @throws Error When template engine is not provided.
	 */
	public function __construct( array $args ) {
		if (
			! $args['template_engine'] ||
			! ( $args['template_engine'] instanceof TemplateEngine )
		) {
			throw new Error( 'Template engine not provided' );
		}

		$this->template_engine                = $args['template_engine'];
		$this->namespace                      = $args['namespace'];
		$this->base_folder                    = $args['base_folder'];
		$this->registrable_namespaces         = isset( $args['registrable_namespaces'] ) ? $args['registrable_namespaces'] : array();
		$this->registrable_namespaces_for_acf = isset( $args['registrable_namespaces_for_acf'] ) ? $args['registrable_namespaces_for_acf'] : array();

		$this->load_registrable_classes( $this->registrable_namespaces );

		// Make sure ACF registrations are done after ACF initialization.
		add_action( 'acf/init', array( $this, 'load_acf_registrable_classes' ) );
	}

	/**
	 * Handle initialization of ACF related classes
	 */
	public function load_acf_registrable_classes(): void {
		$this->load_registrable_classes( $this->registrable_namespaces_for_acf );
	}

	/**
	 * Search in folders for classes that implements Registrable interface,
	 * creates a new instance of those classes and calls the register method.
	 *
	 * @param array $namespaces List of classes namespaces.
	 */
	protected function load_registrable_classes( $namespaces = array() ): void {
		foreach ( $namespaces as $namespace ) {
			$namespace_dir =
				get_template_directory() .
				"/{$this->base_folder}//" .
				$namespace;

			foreach ( scandir( $namespace_dir, 1 ) as $file ) {
				if ( strpos( $file, '.php' ) === false ) {
					continue;
				}

				$class_name_array = explode( '.php', $file );
				$class_name       = array_shift( $class_name_array );
				$class_namespaced =
					$this->namespace . '\\' . $namespace . '\\' . $class_name;
				$class_instance   = new $class_namespaced();
				$class_instance->register();
			}
		}
	}
}