<?php

namespace Solidify\Core;

/**
 * Creates a new interface to ACF Field Group
 */
abstract class CustomField {
	/**
	 * Undocumented variable
	 *
	 * @var Field[] - Field objects array
	 */
	public $fields = array();
}