<?php

new Fields\CloneField(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'conditional_logic' => array(
			array(
				array(
					'field' => '', // Field name, Ex: fieldGroupKey_repeaterOrGroupName_fieldName || fieldGroupKey_fieldName
					'operator' => '', // Define validation Ex: != || == || !=empty || ==empty || ==pattern || ==contains
				),
			),
		), // Array for validation, 0 for null
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'clone' => '', // Select one or more fields you wish to clone
		'display' => 'seamless', //Specify the style used to render the clone field "seamless" or "group"
		'layout' => 'block',
		'prefix_label' => 0, // Define if label will be displayed %field_label% or string_%field_label%
		'prefix_name' => 0, // Define if label will be displayed %field_name% or string_%field_name%
	)
);
