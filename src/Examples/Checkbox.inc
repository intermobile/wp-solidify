<?php

new Fields\Checkbox(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'choices' => array(
		),
		'allow_custom' => 0, // Allow 'custom' values to be added
		'default_value' => '', // Define a default value
		'layout' => 'vertical', // Define correct layout horizontal or vertical
		'toggle' => 0, // Prepend an extra checkbox to toggle all choices
		'return_format' => 'value', // Return value, label or both
		'save_custom' => 0,
	)
);
