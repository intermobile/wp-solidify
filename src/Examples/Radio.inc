<?php

new Fields\Radio(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'conditional_logic' => array(
			array(
				array(
					'field' => '', // Field name, Ex: fieldGroupKey_repeaterOrGroupName_fieldName || fieldGroupKey_fieldName
					'operator' => '', // Define validation Ex: != || == || !=empty || ==empty || ==pattern || ==contains
				),
			),
		), // Array for validation, 0 for null
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'choices' => array(
		), //Enter each choice on a new line.
		'allow_null' => 0,
		'other_choice' => 0, // Add 'other' choice to allow for custom values
		'default_value' => '',
		'layout' => 'vertical', // Define the correct layout vertical or horizontal
		'return_format' => 'value',  // Return value, label or both
		'save_other_choice' => 0, // Save 'other' values to the field's choices
	)
);
