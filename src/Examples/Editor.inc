<?php

new Fields\Editor(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'conditional_logic' => array(
			array(
				array(
					'field' => '', // Field name, Ex: fieldGroupKey_repeaterOrGroupName_fieldName || fieldGroupKey_fieldName
					'operator' => '', // Define validation Ex: != || == || !=empty || ==empty || ==pattern || ==contains
				),
			),
		), // Array for validation, 0 for null
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'default_value' => '', // Start with default value
		'tabs' => 'all', // Define tabs exhibition (all, visual, text)
		'toolbar' => 'full', // Define toolbar type (full, basic)
		'media_upload' => 1, // Allow/Disallow media upload
		'delay' => 0, // TinyMCE will not be initialized until field is clicked
	)
);
