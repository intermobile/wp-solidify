<?php

new Fields\Button(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'message' => '',
		'default_value' => '', // Define a default value
		'ui' => 1, // Define if button will use ui layout
		'ui_on_text' => '', // Define text for button selected
		'ui_off_text' => '', // Define text for button unselected
	)
);
