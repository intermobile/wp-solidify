<?php

new Fields\Textarea(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'conditional_logic' => array(
			array(
				array(
					'field' => '', // Field name, Ex: fieldGroupKey_repeaterOrGroupName_fieldName || fieldGroupKey_fieldName
					'operator' => '', // Define validation Ex: != || == || !=empty || ==empty || ==pattern || ==contains
				),
			),
		), // Array for validation, 0 for null
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'default_value' => '',
		'placeholder' => '',
		'maxlength' => '', // Character Limit. Leave blank for no limit.
		'rows' => '', // Sets the textarea height
		'new_lines' => '', // Controls how new lines are rendered (wpautop, br). Leave blank for no format.
	)
);
