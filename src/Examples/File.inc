<?php

new Fields\File(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'conditional_logic' => array(
			array(
				array(
					'field' => '', // Field name, Ex: fieldGroupKey_repeaterOrGroupName_fieldName || fieldGroupKey_fieldName
					'operator' => '', // Define validation Ex: != || == || !=empty || ==empty || ==pattern || ==contains
				),
			),
		), // Array for validation, 0 for null
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'return_format' => 'array',
		'library' => 'all', // Limit the media library choice all, uploadedTo
		'min_size' => '', // Define min image size in MB
		'max_size' => '', // Define max image size in MB
		'mime_types' => '', // Define permitted mime types
	)
);
