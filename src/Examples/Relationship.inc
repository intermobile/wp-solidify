<?php

new Fields\Relationship(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'conditional_logic' => array(
			array(
				array(
					'field' => '', // Field name, Ex: fieldGroupKey_repeaterOrGroupName_fieldName || fieldGroupKey_fieldName
					'operator' => '', // Define validation Ex: != || == || !=empty || ==empty || ==pattern || ==contains
				),
			),
		), // Array for validation, 0 for null
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'post_type' => '', // Array post, attachment, page, event
		'taxonomy' => '', // Taxonomy name
		'filters' => array(
			0 => 'search',
			1 => 'post_type',
			2 => 'taxonomy',
		),
		'elements' => '', // Selected elements will be displayed in each result (featured_image)
		'min' => '', // Select minimum posts
		'max' => '', // Select maximum posts
		'return_format' => '', // Define return format object, id
	)
);
