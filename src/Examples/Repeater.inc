<?php

new Fields\Repeater(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'conditional_logic' => array(
			array(
				array(
					'field' => '', // Field name, Ex: fieldGroupKey_repeaterOrGroupName_fieldName || fieldGroupKey_fieldName
					'operator' => '', // Define validation Ex: != || == || !=empty || ==empty || ==pattern || ==contains
				),
			),
		), // Array for validation, 0 for null
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'collapsed' => '',
		'min' => 0, // Min lines
		'max' => 0, // Max lines
		'layout' => 'table', // Define the correct layout block or table or row
		'button_label' => '', // Define the correct add row button label
		'sub_fields' => array(
		), // Define sub-fields for repeater
	)
);
