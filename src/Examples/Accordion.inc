<?php

new Fields\Accordion(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'open' => 0, // Displays this accordion as open when loading the page.
		'multi_expand' => 0, // Allow this accordion to open without closing others.
		'endpoint' => 0, // Set an end point for the previous accordion to stop.
	)
);
