<?php

new Fields\Taxonomy(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'conditional_logic' => array(
			array(
				array(
					'field' => '', // Field name, Ex: fieldGroupKey_repeaterOrGroupName_fieldName || fieldGroupKey_fieldName
					'operator' => '', // Define validation Ex: != || == || !=empty || ==empty || ==pattern || ==contains
				),
			),
		), // Array for validation, 0 for null
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'taxonomy' => 'category', // Define taxonomy type (category, post_tag, post_format)
		'field_type' => 'checkbox', // Define field type (checkbox, multi_select, radio, select)
		'add_term' => 1, // Allow new terms to be created whilst editing
		'save_terms' => 0, // Connect selected terms to the post
		'load_terms' => 0, // Load value from posts terms
		'return_format' => 'id', // Define the return format (object, id)
		'multiple' => 0,
		'allow_null' => 0,
	)
);
