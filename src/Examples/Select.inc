<?php

new Fields\Select(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'conditional_logic' => array(
			array(
				array(
					'field' => '', // Field name, Ex: fieldGroupKey_repeaterOrGroupName_fieldName || fieldGroupKey_fieldName
					'operator' => '', // Define validation Ex: != || == || !=empty || ==empty || ==pattern || ==contains
				),
			),
		), // Array for validation, 0 for null
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'choices' => array(
		), // Array of choices for select
		'default_value' => false, // The default selected file
		'allow_null' => 0,
		'multiple' => 0, // Define if the user can choose one option or infinite options
		'ui' => 0, // Define if select will use ui layout
		'return_format' => 'value', // Return value, label or both
		'ajax' => 0,
	)
);
