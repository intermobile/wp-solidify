<?php

new Fields\Image(
	'', // Field label
	array(
		'instructions' => '', // Field instructions
		'required' => 0, // Define if field is required | 0 - not required / 1 - required
		'conditional_logic' => array(
			array(
				array(
					'field' => '', // Field name, Ex: fieldGroupKey_repeaterOrGroupName_fieldName || fieldGroupKey_fieldName
					'operator' => '', // Define validation Ex: != || == || !=empty || ==empty || ==pattern || ==contains
				),
			),
		), // Array for validation, 0 for null
		'wrapper' => array(
			'width' => '', // Field size
			'class' => '', // Field class
			'id' => '', // Field id
		),
		'return_format' => 'array', // Define return format array or id or url
		'preview_size' => 'medium', // Define the preview size thumbnail, medium, large, medium_large, 1536x1536, 2048x2048, full
		'library' => 'all', // Limit the media library choice all, uploadedTo
		'min_width' => '', // Define min width in PX
		'min_height' => '', // Define min height in PX
		'min_size' => '', // Define min image size in MB
		'max_width' => '', // Define max width in PX
		'max_height' => '', //Define max height in PX
		'max_size' => '', // Define max image size in MB
		'mime_types' => '', // Define permitted mime types
	)
);
